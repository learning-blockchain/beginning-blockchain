const Web3 = require('web3');

const INFURA_API_KEY = '<your infura API key>';
const INFURA_API_PATH = `https://ropsten.infura.io/${INFURA_API_KEY}`;

const web3 = new Web3(new Web3.providers.HttpProvider(INFURA_API_PATH));

const createAccounts = function () {
  const aliceKeys = web3.eth.accounts.create();
  console.log(aliceKeys);
  const bobKeys = web3.eth.accounts.create();
  console.log(bobKeys);
};

const getBalance = function () {
  // use your own generated account address
  web3.eth.getBalance('0xAff9d328E8181aE831Bc426347949EB7946A88DA').then(console.log);
};

const signTransaction = function () {
  const tx = {
    from: "0xAff9d328E8181aE831Bc426347949EB7946A88DA",
    gasPrice: "20000000000",
    gas: "42000",
    to: '0x22013fff98c2909bbFCcdABb411D3715fDB341eA',
    value: "1000000000000000000",
    data: ""
  };
  web3.eth.accounts.signTransaction(tx, '0x9fb71152b32cb90982f95e2b1bf2a5b6b2a53855eacf59d132a2b7f043cfddf5')
    .then(function(signedTx){
      console.log(signedTx.rawTransaction);
    });
};
