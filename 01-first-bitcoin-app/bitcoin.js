const btc = require('bitcoinjs-lib');
const request = require('request');

const network = btc.networks.testnet;
const blockExplorerTestnetApiEndpoint = 'https://testnet.blockexplorer.com/api/';

const getKeys = function () {
  const aliceKeys = btc.ECPair.makeRandom({
    network
  });
  const bobKeys = btc.ECPair.makeRandom({
    network
  });
  const alicePublic = aliceKeys.getAddress();
  const alicePrivate = aliceKeys.toWIF();
  const bobPublic = bobKeys.getAddress();
  const bobPrivate = bobKeys.toWIF();
  console.log(alicePublic, alicePrivate, bobPublic, bobPrivate);
};

const getOutputs = function(address) {
  const url = `${blockExplorerTestnetApiEndpoint}addr/${address}/utxo`;
  return new Promise(function(resolve, reject) {
    request.get(url, function (err, res, body) {
      if (err) {
        reject(err);
      }
      resolve(body);
    });
  });
};

const createTransaction = function () {
  const alice = btc.ECPair.fromWIF('cQQhHcH6C7A9VNFtGdwaSwNDRpNHDfYrPc3XnS9Puz4HHU4axM2z', network);
  const bob = btc.ECPair.fromWIF('cW1MB2G86LqJ9sy8cUeuxotmuaU5LyMMZffvpaFxbUw144dj1QpP', network);

  getOutputs(bob.getAddress()).then(function (res) {
    const utxo = JSON.parse(body.toString());

    const transaction = new btc.TransactionBuilder(network);
    transaction.addInput(utxo[0].txid, utxo[0].vout);
    transaction.addOutput(alice.getAddress(), 99992000);
    transaction.sign(0, bob);

    const transactionHex = transaction.build().toHex();
    const txPushUrl = blockExplorerTestnetApiEndpoint + 'tx/send';
    request.post({
      url: txPushUrl,
      json: {
        rawtx: transactionHex
      }, function (err, res, body) {
        if (err) console.log(err);
        console.log(res);
        console.log(body);
      }
    });
  });
};

module.exports = {
  getKeys,
  createTransaction
};
